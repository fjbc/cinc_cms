#! /usr/bin/env python
# -*- coding: utf8 -*-

def nombre_archivo_web2py(tablename,columnname,columnlength,filename):
	import os, uuid, base64, re
	filename = os.path.basename(filename.replace('/', os.sep).replace('\\', os.sep))
	REGEX_STORE_PATTERN = re.compile('\.(?P<e>\w{1,5})$')
	m = REGEX_STORE_PATTERN.search(filename)
	extension = m and m.group('e') or 'txt'
	uuid_key = str(uuid.uuid4()).replace('-', '')[-16:]
	encoded_filename = base64.b16encode(filename).lower()
	newfilename = '%s.%s.%s.%s' % \
		(tablename, columnname, uuid_key, encoded_filename)
	newfilename = newfilename[:(columnlength - 1 - len(extension))] + '.' + extension
	return newfilename

def procesar_foto(db,row,idr=None):
    #Revisa si te mandaron foto
    if row.vars['foto_personal']:
        from PIL import Image, ImageOps
        from base64 import b16decode
        im=Image.open(db.investigadores.foto_personal.uploadfolder+'/'+row.vars['foto_personal'])
        #Primero genera la miniatura en 350x350
        thumb=ImageOps.fit(im,(350,350),Image.ANTIALIAS)
        iname=b16decode(row.vars['foto_personal'].split(".")[-2].upper())
        tname=nombre_archivo_web2py('investigadores','thumb_personal',
            db.investigadores.thumb_personal.length,'thumb_'+iname)
        thumb.save(db.investigadores.foto_personal.uploadfolder+'/'+tname)
        #Luego cambia el tamaño del original a un máximo de 1920x1080
        im.thumbnail((1920,1080),Image.ANTIALIAS)
        im.save(db.investigadores.foto_personal.uploadfolder+'/'+row.vars['foto_personal'])
        if not idr:
            db(db.investigadores.id==row.vars['id']).update(thumb_personal=tname)
        else:
            db(db.investigadores.id==idr).update(thumb_personal=tname)
    else:
        print "No mandaron foto"
