#! /usr/bin/env python
# -*- coding: utf8 -*-

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Anuncios'))
def index():
    grid=SQLFORM.grid(db.anuncios,showbuttontext=False,csv=False,details=False)
    for e in grid.elements('input#anuncios_titulo'):
	e['_maxlength']=db.anuncios.titulo.length
    for e in grid.elements('input#anuncios_texto_corto'):
	e['_maxlength']=db.anuncios.texto_corto.length
    for e in grid.elements('textarea#anuncios_texto'):
	e['_maxlength']=db.anuncios.texto.length
    return dict(grid=grid)
