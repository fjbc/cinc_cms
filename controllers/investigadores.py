#! /usr/bin/env python
# -*- coding: utf8 -*-

def index():
    """Listado completo de investigadores con búsqueda"""
    invs=db(db.investigadores.id_usuario==db.auth_user.id).select(orderby=db.investigadores.departamento)
    response.title=T('Researchers')
    return dict(invs=invs)

@auth.requires_membership('Administrador')
def editar():
    """Formulario para poder ver y editar los datos de los investigadores"""
    from utilidades import procesar_foto
    grid=SQLFORM.grid(db.investigadores,showbuttontext=False,
        onupdate=lambda r:procesar_foto(db,r))
    return dict(grid=grid)

def detalles():
    if not request.vars.id:
        redirect(URL('investigadores','index'))
    inv=db.investigadores(int(request.vars.id))
    inv_user=db.auth_user(inv.id_usuario)
    if not inv:
        raise HTTP(404,"Investigador no encontrado")
    pubs=db(db.publicaciones.lista_investigadores.contains(inv_user.id)).select(orderby=~db.publicaciones.anio)
    return dict(inv=inv,inv_user=inv_user,pubs=pubs)
