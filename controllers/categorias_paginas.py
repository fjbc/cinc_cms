#! /usr/bin/env python
# -*- coding: utf8 -*-

@auth.requires_membership('Administrador')
def index():
    grid=SQLFORM.grid(db.categorias_paginas,showbuttontext=False,
        searchable=False,
        csv=False,deletable=False,details=False)
    return dict(grid=grid)
