#! /usr/bin/env python
# -*- coding: utf8 -*-

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def index():
    """Función para listar todos los artículos del CMS"""
    db.paginas.nombre_url.readable=True
    grid=SQLFORM.grid(db.paginas,showbuttontext=False,create=False,editable=False,csv=False,details=False,
        links=[
            lambda row:A(SPAN(_class="icon pen icon-pencil glyphicon glyphicon-pencil"),_class="button btn btn-default", _title=T("Edit page"),
                _href=URL("cms","editar",vars={'pid':row.id}))
        ])
    boton_nuevo=A(SPAN(_class="icon plus icon-plus glyphicon glyphicon-plus"),_class="button btn btn-default", _title=T("Create new page"),
        _href=URL('cms','crear_nueva_pagina'))
    print grid[0]
    grid[0].insert(0,boton_nuevo)
    return dict(grid=grid)

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def crear_nueva_pagina():
    """Función que crea una nueva página y redirecciona a la vista de editar.
    Permite poder insertarle imagenes inmediatamente a diferencia de si uso la vista
    normal del grid create."""
    pid=db.paginas.insert(titulo="Nueva página",publicado=False)
    print pid
    redirect(URL('cms','editar',vars={'pid':pid}))

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def editar():
    """Función para editar las páginas permite subir imagenes y cambiar el contenido"""
    try:
        p=db.paginas(request.vars.pid)
    except:
        raise HTTP(404,T("Selected page not found"))
    if not p:
        raise HTTP(404,T("Selected page not found"))
    db.paginas.id.readable=False
    if p.categoria and p.categoria.en_menu:
        db.paginas.en_menu.writable=True
    form=SQLFORM(db.paginas,p,formstyle="bootstrap")
    form.process(next=URL('cms','index'),message_onsuccess=T("Page saved successfully"))
    return dict(form=form,p=p)

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def subir_imagen():
    if not request.vars.pid:
        return "No específico a que página pertenece la imagen"
    p=db.paginas(int(request.vars.pid))
    if not p:
        return "La página especificada no existe"
    if request.vars.get('file')==None:
        return "No seleccionó imagen"
    #Obtener el nombre de archivo
    nombre_archivo=request.vars.file.filename
    #Revisa que exista la ruta para el post
    from os import path, makedirs
    path_imgs=path.join(request.folder,'static','page_media','page%d'%p.id)
    if not path.isdir(path_imgs):
        makedirs(path_imgs)
    #Ahora redimensiona la imagen
    from PIL import Image
    #from herramientas import nombre_archivo_web2py
    try:
        im=Image.open(request.vars.file.file)
    except:
        return "No mando un archivo de imagen soportado"
    im.thumbnail((1280,800),Image.ANTIALIAS)
    im_filename=nombre_archivo_web2py('imagenes_paginas','imagen',db.imagenes_paginas.imagen.length,nombre_archivo)
    i_ancho,i_alto=im.size
    im.save(path_imgs+'/'+im_filename)
    fid=db.imagenes_paginas.insert(id_pagina=p.id,imagen=im_filename,i_ancho=i_ancho,i_alto=i_alto)
    url_img=URL('static','page_media/page%d/%s'%(p.id,im_filename),scheme=True,host=True)
    return url_img


def nombre_archivo_web2py(tablename,columnname,columnlength,filename):
	import os, uuid, base64, re
	filename = os.path.basename(filename.replace('/', os.sep).replace('\\', os.sep))
	REGEX_STORE_PATTERN = re.compile('\.(?P<e>\w{1,5})$')
	m = REGEX_STORE_PATTERN.search(filename)
	extension = m and m.group('e') or 'txt'
	uuid_key = str(uuid.uuid4()).replace('-', '')[-16:]
	encoded_filename = base64.b16encode(filename).lower()
	newfilename = '%s.%s.%s.%s' % \
		(tablename, columnname, uuid_key, encoded_filename)
	newfilename = newfilename[:(columnlength - 1 - len(extension))] + '.' + extension
	return newfilename

@auth.requires_membership('Administrador')
def menus():
    grid=SQLFORM.grid(db.menus,showbuttontext=False,details=False,csv=False,searchable=False)
    return dict(grid=grid)

def pagina():
    try:
        p=db(db.paginas.nombre_url==request.vars.p).select().first()
    except:
        raise HTTP(404,T('Page not found'))
    if not p:
        raise HTTP(404,T('Page not found'))
    response.title=p.titulo
    return dict(p=p)
