#! /usr/bin/env python
# -*- coding: utf8 -*-
@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def index():
    grid=SQLFORM.grid(db.publicaciones,showbuttontext=False)
    return dict(grid=grid)

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Publicaciones'))
def tipos():
    grid=SQLFORM.grid(db.tipos_publicaciones,showbuttontext=False)
    return dict(grid=grid)

@auth.requires_membership('Investigador')    
def mis_publicaciones():
    db.publicaciones.lista_investigadores.default=[auth.user.id]
    grid=SQLFORM.grid(db.publicaciones.lista_investigadores.contains(auth.user.id),
        showbuttontext=False,csv=False,deletable=False,
        links=[
            lambda row: A(I(_class="fa fa-ban"),_title=T('Remove from my articles'),
                _class="btn btn-default",
                _onclick="return confirm('"+T('Are you sure you want to remove this article?')+"')",
                _href=URL('publicaciones','quitar_de_mis_publicaciones',vars={'pid':row.id}))
        ])
    return dict(grid=grid)

@auth.requires_membership('Investigador')    
def quitar_de_mis_publicaciones():
    if not request.vars.pid:
        session.flash=T("No publication selected")
        redirect(URL('publicaciones','mis_publicaciones'))
    pub=db.publicaciones(int(request.vars.pid))
    li=pub.lista_investigadores
    if auth.user.id in li:
        li.remove(auth.user.id)
        db(db.publicaciones.id==pub.id).update(lista_investigadores=li)
        session.flash=T("Publication removed")
    else:
        session.flash=T("Nothing done")
    redirect(URL('publicaciones','mis_publicaciones'))

def listar():
    vars_req={}
    ini=0
    if request.vars.ini:
        ini=int(request.vars.ini)
        vars_req['ini']=ini
    ppp=20
    if request.vars.ppp:
        ppp=int(request.vars.ppp)
        vars_req['ppp']=ppp
    area=None
    query=None
    invs_area=None
    if request.vars.area:
        area=request.vars.area
        if area in ['Matemáticas','Física','Computación y Robótica']:
            invs_area=db((db.investigadores.departamento==area)&
                     (db.auth_user.id==db.investigadores.id_usuario)).select(db.auth_user.id)
            for inv in invs_area:
                if query==None:
                    query=db.publicaciones.lista_investigadores.contains(inv.id)
                else:
                    query|=db.publicaciones.lista_investigadores.contains(inv.id)
    if query==None:
       query=db.publicaciones.anio==2015
    pubs=db(query).select(orderby=~db.publicaciones.anio, 
        limitby=(ini,ini+ppp))
    total=db(query).count()
    response.title=T('Publications')
    return dict(pubs=pubs,ini=ini,ppp=ppp,total=total,area=area)

@auth.requires(auth.has_membership('Investigador') or auth.has_membership('Administrador'))
def editar():
    if not request.vars.pid:
        session.flash=T('No publication selected')
        redirect(URL('publicaciones','index'))
    print request.vars.pid
    pub=db.publicaciones(int(request.vars.pid))
    form=SQLFORM(db.publicaciones,pub)
    if form.process().accepted:
        session.flash=T('Publication saved')
        redirect(URL('publicaciones','listar'))
    return dict(form=form)


