# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    #Listado de investigadores al azar para la portada
    invs=db(db.investigadores).select(orderby='<random>',limitby=(0,4))
    #Los eventos que siguen
    evs=db((db.eventos.fecha_inicial>=request.now)|(db.eventos.fecha_final>=request.now)).select(orderby=db.eventos.fecha_inicial,limitby=(0,5))
    anuncios=db(db.anuncios).select(orderby=~db.anuncios.id,limitby=(0,5))
    notas=db(db.notas_portada).select(orderby='<random>',limitby=(0,3))
    ligas=db(db.ligas_de_interes).select(orderby='<random>',limitby=(0,6))
    return dict(invs=invs,evs=evs,anuncios=anuncios,notas=notas,ligas=ligas)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    if 'profile' in request.args and auth.has_membership('Investigador'):
        db.auth_user.password.writable=False
        db.auth_user.email.writable=False
        db.auth_user.email.readable=False
        db.investigadores.id_usuario.readable=False
        #Revisa si el perfil del investigador existe
        inv=db(db.investigadores.id_usuario==auth.user.id).select().first()
        if not inv:
            id_inv=db.investigadores.insert(id_usuario=auth.user.id)
            inv=db.investigadores(id_inv)
        form=SQLFORM.factory(db.auth_user,db.investigadores,table_name="investigadores")
        form2=SQLFORM(db.investigadores,inv)
        print dir(form2)
        form.vars.gender=auth.user.gender
        form.vars.first_name=auth.user.first_name
        form.vars.last_name=auth.user.last_name
        form.vars.departamento=inv.departamento
        form.vars.nivel=inv.nivel
        form.vars.sni=inv.sni
        form.vars.lineas_de_investigacion=inv.lineas_de_investigacion
        form.vars.extension=inv.extension
        form.vars.url_facebook=inv.url_facebook
        form.vars.url_twitter=inv.url_twitter
        form.vars.url_linkedin=inv.url_linkedin
        form.vars.url_youtube=inv.url_youtube
        if inv.foto_personal:
            #Muestra la foto
            print "Hay foto"
            foto_row=foto_row=form.elements('#investigadores_foto_personal__row',first_only=True)
            if len(foto_row)==1:
                print "Hay un div con este id"
                foto_div=foto_row[0].elements('.col-sm-9',first_only=True)
                if len(foto_div)==1:
                    print "Hay sub div"
                    foto_div[0].append(IMG(_src=URL('static','uploads/'+inv.foto_personal),_style="width:150px;"))
        if form.accepts(request.vars):
            print "Actualizando:",form.vars
            db(db.auth_user.id==auth.user.id).update(
                gender=form.vars.gender,
                first_name=form.vars.first_name,
                last_name=form.vars.last_name
            )
            from utilidades import procesar_foto
            procesar_foto(db,form,inv.id)
            auth.user.update(gender=form.vars.gender,first_name=form.vars.first_name,last_name=form.vars.last_name)
            auth.user=db.auth_user(auth.user.id)
            db(db.investigadores.id==inv.id).update(
                departamento=form.vars.departamento,
                nivel=form.vars.nivel,
                sni=form.vars.sni,
                lineas_de_investigacion=form.vars.lineas_de_investigacion,
                extension=form.vars.extension,
                url_facebook=form.vars.url_facebook,
                url_twitter=form.vars.url_twitter,
                url_linkedin=form.vars.url_linkedin,
                url_youtube=form.vars.url_youtube,
                foto_personal=form.vars.foto_personal,
            )
            session.flash=T('Profile updated')
            redirect(URL('default','index'))
        return dict(form=form)
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


