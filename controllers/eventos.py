#! /usr/bin/env python
# -*- coding: utf8 -*-

@auth.requires(auth.has_membership('Administrador') or auth.has_membership('Eventos'))
def index():
    grid=SQLFORM.grid(db.eventos,showbuttontext=False,csv=False,details=False)
    for e in grid.elements('input#eventos_nombre'):
        e['_maxlength']=db.eventos.nombre.length
    for e in grid.elements('input#eventos_texto_corto'):
        e['_maxlength']=db.eventos.texto_corto.length
    for e in grid.elements('textarea#eventos_texto_largo'):
        e['_maxlength']=db.eventos.texto_largo.length
    return dict(grid=grid)
