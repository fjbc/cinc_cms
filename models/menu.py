# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(IMG(_src=URL('static','images/logo_cinc.png')),_class="navbar-brand",_href="/",_style="padding: 2px 15px;")
response.title = request.application.replace('_',' ').title()
response.subtitle = ''

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Your Name <you@example.com>'
response.meta.description = 'a cool new app'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Home'), False, URL('default', 'index'), []),
    (T('Research'), False, None, [
        (T('Researchers'),False,URL('investigadores','index'),[]),
        (T('Publications'),False,URL('publicaciones','listar'),[]),
        ('Listado de Publicaciones en PDF',False,URL('cms','pagina',vars={'p':'lista-de-publicaciones-en-pdf'}),[]),
    ])]

cat_en_menu=db(db.categorias_paginas.en_menu==True).select()
for cat in cat_en_menu: 
    submenu=[]
    pags_en_menu=db((db.paginas.categoria==cat.id)&
                    (db.paginas.publicado==True)).select()
    for pag in pags_en_menu:
        submenu.append((pag.titulo,False,URL('cms','pagina',vars={'p':pag.nombre_url})))
    if len(submenu)>0:    
        response.menu.append((cat.nombre,False,None,submenu))

response.menu.append(
    ('Oferta educativa',False,None,[
      ('Licenciatura en Ciencias',False,'http://www.fc.uaem.mx/licenciatura-en-ciencias/',[]),
      ('Posgrado en Ciencias',False,'http://www.iicba.uaem.mx/init/oferta_educativa/ver?oferta=4',[]),
    ])
)

response.menu.append(('Contacto',False,URL('cms','pagina',vars={'p':'contacto'}),[]))

edit_menu=[]
if auth.user and (auth.has_membership('Administrador') or auth.has_membership('Publicaciones')):    
    edit_menu.append( (T('Page Categories'),False,URL('categorias_paginas','index'),[]) )
    edit_menu.append( (T('Pages'),False,URL('cms','index'),[]) )
    edit_menu.append( (T('Menu'),False,URL('cms','menus'),[]) )
    edit_menu.append( (T('Publications'),False,URL('publicaciones','index'),[]) )
    edit_menu.append( (T('Type of Publications'),False,URL('publicaciones','tipos'),[]) )

if auth.user and (auth.has_membership('Administrador') or auth.has_membership('Eventos')):
    edit_menu.append( (T('Events'),False,URL('eventos','index'),[]) )

if auth.user and (auth.has_membership('Administrador') or auth.has_membership('Anuncios')):
    edit_menu.append( (T('Anouncements'),False,URL('anuncios','index'),[]) )

if auth.user and auth.has_membership('Administrador'):
    edit_menu.append( (T('Researchers'),False,URL('investigadores','editar'),[]) )
    edit_menu.append( (T('Home Notes'),False,URL('notas_portada','index'),[]) )
    edit_menu.append( (T('Interesting Links'),False,URL('ligas','index'),[]) )

if len(edit_menu)>0:
    response.menu.append((T('Edit'),False,None,edit_menu))
