# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

## app configuration made easy. Look inside private/appconfig.ini
from gluon.contrib.appconfig import AppConfig
## once in production, remove reload=True to gain full speed
myconf = AppConfig(reload=True)


if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL(myconf.take('db.uri'), pool_size=myconf.take('db.pool_size', cast=int), check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## choose a style for forms
response.formstyle = myconf.take('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.take('forms.separator')


## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################
import locale
locale.setlocale(locale.LC_ALL,'es_MX.UTF-8')

from gluon.tools import Auth, Service, PluginManager

auth = Auth(db)
service = Service()
plugins = PluginManager()

## create all tables needed by auth if not custom tables
auth.settings.extra_fields['auth_user']= [
  Field('gender',label=T('Gender'),length=1),
  ]
auth.define_tables(username=False, signature=False)
db.auth_user.gender.requires = IS_IN_SET(['F','M'])
## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.take('smtp.server')
mail.settings.sender = myconf.take('smtp.sender')
mail.settings.login = myconf.take('smtp.login')

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True
auth.settings.create_user_groups = False

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

db.define_table('categorias_paginas',
    Field('nombre'),
    Field('descripcion',type='text'),
    Field('en_menu',type="boolean",default=False))

db.define_table('paginas',
    Field('titulo',length=128,notnull=True,unique=False),
    Field('contenido', type="text",readable=False,length=20000),
    Field('categoria', db.categorias_paginas,
        represent=lambda categoria,row: categoria and categoria.nombre or "-"),
    Field('publicado', type="boolean",default=True),
    Field('en_menu',type="boolean",default=False,
        writable=False,readable=False),
    Field('nombre_url',length=140,unique=True,readable=False),
	auth.signature)
db.paginas.nombre_url.compute=lambda row: IS_SLUG()(row['titulo'])[0]
db.paginas.categoria.requires = IS_NULL_OR(IS_IN_DB(db,db.categorias_paginas.id,"%(nombre)s"))

db.define_table('imagenes_paginas',
    Field('id_pagina',db.paginas,ondelete='CASCADE'),
    Field('imagen',type="upload",autodelete=True,
          uploadfolder=request.folder+'static/uploads',
          readable=False,writable=False),
    Field('i_ancho',type="integer",writable=False,readable=False),
    Field('i_alto',type="integer",writable=False,readable=False),
    )

db.define_table('investigadores',
    Field('id_usuario',db.auth_user,
        writable=False),
    Field('departamento',length=30),
    Field('nivel'),
    Field('sni',label="SNI"),
    Field('lineas_de_investigacion',type="text"),
    Field('investigacion_actual',type="text"),
    Field('extension',length=6),
    Field('formacion',type="text"),
    Field('url_facebook',readable=False),
    Field('url_twitter',readable=False),
    Field('url_linkedin',readable=False),
    Field('url_youtube',readable=False),
    #Las imagenes propongo 2 de momento sólo una
    Field('foto_personal',type="upload",readable=False,
        autodelete=True,uploadfolder=request.folder+'static/uploads'),
    Field('thumb_personal',type="upload",readable=False,writable=False,
        autodelete=True,uploadfolder=request.folder+'static/uploads'),
)

db.investigadores.departamento.requires = IS_IN_SET(['Computación y Robótica','Física','Matemáticas'])

db.define_table('tipos_publicaciones',
    Field('tipo'),
    #ToDo Una Mascara para elegir que campos mostrar y cuales ocultar en base a el tipo
)

db.define_table('publicaciones',
    Field('cita',length=1000,label=T('Citation')),
    Field('tipo',type="reference tipos_publicaciones"),
    Field('titulo',length=500,label=T('Title')),
    Field('autores',length=500,label=T('Author')),
    Field('editores',length=500,label=T('Editor'),readable=False),
    Field('nombre_publicacion',label=T("Publication Name")),
    Field('anio',label=T('Year')),
    Field('mes',label=T('Month')),
    Field('volumen',label=T('Volume'),readable=False),
    Field('paginas',label=T('Pages'),length=15,readable=False),
    Field('capitulo',label=T('Chapter'),readable=False),
    Field('edicion',label=T('Edition'),readable=False),
    Field('issn_isbn',length=75,readable=False),
    Field('doi',readable=False),
    Field('indizada_jcr',type="boolean"),
    Field('incluye_alumnos',type="boolean"),
    Field('resumen',type="text",label=T('Abstract')+" ("+T('Spanish')+")"),
    Field('abstract',type="text",label=T('Abstract')+" ("+T('English')+")"),
    Field('documento',type="upload"),
    Field('lista_investigadores',type="list:reference auth_user"),
    )

db.publicaciones.tipo.requires=IS_IN_DB(db,db.tipos_publicaciones.id,'%(tipo)s')
#db.publicaciones.lista_investigadores.requires=IS_LIST_OF(IS_IN_DB(db,db.auth_user.id,'%(first_name)s %(last_name)s'))#,multiple=True    
db.publicaciones.lista_investigadores.requires=IS_IN_DB(db(db.investigadores.id_usuario==db.auth_user.id),db.auth_user.id,'%(first_name)s %(last_name)s',multiple=True)



db.define_table('menus',
    Field('nombre',length=128,notnull=True),
    Field('pagina',db.paginas,notnull=True,represent= lambda i,r:r.pagina.titulo),
    Field('padre', type='reference menus',represent=lambda i,r:(r.padre and r.padre.nombre) or ''),
    Field('publico',type="boolean",default=True),
    Field('orden',type="integer",default=0,represent=lambda i,r:r.orden or ''))

db.menus.pagina.requires=IS_IN_DB(db,db.paginas,'%(titulo)s')
db.menus.padre.requires=IS_EMPTY_OR(IS_IN_DB(db,db.menus,'%(nombre)s'))

db.define_table('imagenes_portada',
    Field('imagen',type="upload",
        uploadfolder=request.folder+'static/uploads'),
    Field('texto',length=40),
)

db.define_table('eventos',
    Field('nombre'),
    Field('texto_corto',length="200"),
    Field('texto_largo',type="text"),
    Field('lugar'),
    Field('fecha_inicial',type="date",default=request.now),
    Field('hora_inicial',type="time"),
    Field('fecha_final',type="date"),
    Field('hora_final',type="time"),
    Field('url_externa',type="boolean",default=False,readable=False),
    Field('pagina',db.paginas,readable=False),
    Field('url',readable=False)
)

db.eventos.pagina.requires=IS_EMPTY_OR(IS_IN_DB(db,db.paginas,'%(titulo)s'))
db.eventos.pagina.show_if = (db.eventos.url_externa==False)
db.eventos.url.show_if = (db.eventos.url_externa==True)

db.define_table('anuncios',
    Field('titulo'),
    Field('texto_corto',length="200"),
    Field('texto',type="text"),
    Field('imagen',type="upload",uploadfolder=request.folder+'static/uploads/'),
    Field('thumb',type="upload", uploadfolder=request.folder+'static/uploads/'),
    Field('url_externa',type="boolean",default=False,readable=False),
    Field('pagina',db.paginas,readable=False),
    Field('url',readable=False)
)

db.anuncios.pagina.requires=IS_EMPTY_OR(IS_IN_DB(db,db.paginas,'%(titulo)s'))
db.anuncios.pagina.show_if = (db.anuncios.url_externa==False)
db.anuncios.url.show_if = (db.anuncios.url_externa==True)

from smarthumb import SMARTHUMB
db.anuncios.thumb.compute = lambda row: SMARTHUMB(row.imagen,db.anuncios.thumb.uploadfolder,(200,200))

db.define_table('notas_portada',
    Field('nombre'),
    Field('descripcion',type="text"),
    Field('imagen',type='upload',uploadfolder=request.folder+'static/uploads'),
    Field('url'),
)

db.define_table('ligas_de_interes',
    Field('nombre'),
    Field('url'),
    Field('imagen',type="upload",uploadfolder=request.folder+'static/uploads'),
)
